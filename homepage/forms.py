from django import forms
from .models import MataKuliah,Kegiatan,Pengikut

class MataKuliahForms(forms.ModelForm):
    class Meta : 
        model = MataKuliah
        fields = ["namaMatkul","namaDosen","jumlahSks","descMatkul","semester","ruangKelas"]
        widgets = {
            'namaMatkul':forms.TextInput(attrs={'class':'form-control'}), 
            'namaDosen':forms.TextInput(attrs={'class':'form-control'}),
            'jumlahSks':forms.NumberInput(attrs={'class':'form-control'}),
            'descMatkul':forms.TextInput(attrs={'class':'form-control'}),
            'semester':forms.NumberInput(attrs={'class':'form-control'}),
            'ruangKelas':forms.TextInput(attrs={'class':'form-control'})}
    
class KegiatanForms(forms.ModelForm):
    class Meta : 
        model = Kegiatan
        fields = ["namaKegiatan"]
        widgets = {
            'namaKegiatan':forms.TextInput(attrs={'class':'form-control'}),}
            
class PengikutForms(forms.ModelForm):
    class Meta : 
        model = Pengikut
        fields = ["namaPengikut"]
        widgets = {
            'namaPengikut':forms.TextInput(attrs={'class':'form-control'}),}


    
    