from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.htmlcode, name='HTMLcode'),
    path('contact/', views.contact, name='contact'),
    # dilanjutkan ...
    path('hobby/', views.hobby, name='hobby'),
    path('pendidikan/', views.pendidikan, name='pendidikan'),
    path('matakuliah/', views.MatKul, name='matakuliah'),
    path("kegiatan/",views.kegiatan, name='kegiatan'),
    path("addpengikut/<id_add>",views.addpengikut, name='addpengikut'),
    path("searchbarbooks/",views.books,name='books'),
    path("delete/<id_delete>",views.delete, name='delete'),    
    path("signup/",views.signup, name ='signup'),
    path("signin/",views.signin, name ='signin'),
    path("logout/",views.logout1, name ='logout'),
]