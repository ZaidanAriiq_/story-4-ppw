from django.test import TestCase
from django.test import Client
from django.urls import reverse, resolve
from .import models, views, forms
# Create your tests here.
class UnitTest(TestCase):
    def test_createmodels(self):
        matkul = models.MataKuliah.objects.create(namaMatkul = "DDP2",namaDosen = "Hafizd",jumlahSks = 4,descMatkul = "belajar java",semester = 3,ruangKelas = "J")
        kegiatan = models.Kegiatan.objects.create(namaKegiatan = "SBF")
        pengikut = models.Pengikut.objects.create(namaPengikut = "dsa",kegiatan = kegiatan)
        self.assertEqual(matkul.namaMatkul,"DDP2")
        self.assertEqual(kegiatan.namaKegiatan,"SBF")
        self.assertEqual(pengikut.namaPengikut,"dsa")
        
    def test_getHTMLcode(self):
        client = Client()
        url = reverse('homepage:HTMLcode')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'story3HTMLcode.html')
        
    def test_getHOBBY(self):
        client = Client()
        url = reverse('homepage:hobby')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'story3HOBBY.html') 
           
    def test_getCONTACT(self):
        client = Client()
        url = reverse('homepage:contact')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'story3CONTACT.html')
        
    def test_getPENDIDIKAN(self):
        client = Client()
        url = reverse('homepage:pendidikan')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'story3PENDIDIKAN.html')
        
    def test_getMatKul(self):
        client = Client()
        url = reverse('homepage:matakuliah')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'story5MatKul.html')
        
    def test_Kegiatan(self):
        client = Client()
        url = reverse('homepage:kegiatan')
        response = client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'Story6Kegiatan.html')
    
    def test_postKegiatan(self):
        data = {'namaKegiatan' : 'sbf'}
        form = forms.KegiatanForms(data = data)
        self.assertTrue(form.is_valid())
        client = Client()
        url = reverse('homepage:kegiatan')
        request = client.post(url,data = data)
        self.assertEqual(request.status_code,302)
        
    def test_postMatKul(self):
        data = {"namaMatkul" : "DDP2","namaDosen" : "Hafizd","jumlahSks" : 4,"descMatkul" : "belajar java","semester" : 3,"ruangKelas" : "J"}
        form = forms.MataKuliahForms(data = data)
        self.assertTrue(form.is_valid())
        client = Client()
        url = reverse('homepage:matakuliah')
        request = client.post(url,data = data)
        self.assertEqual(request.status_code,302)
    
    def test_delete(self):
        matkul = models.MataKuliah.objects.create(namaMatkul = "DDP2",namaDosen = "Hafizd",jumlahSks = 4,descMatkul = "belajar java",semester = 3,ruangKelas = "J")
        client = Client()
        url = reverse('homepage:delete', args=[matkul.id])
        response = client.get(url)
        self.assertEqual(response.status_code,302)            