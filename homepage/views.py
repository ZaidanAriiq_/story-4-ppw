from django.shortcuts import render,redirect
from .models import MataKuliah,Kegiatan,Pengikut
from .forms import MataKuliahForms,KegiatanForms,PengikutForms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def contact(request):
    return render(request, 'story3CONTACT.html')
def hobby(request):
    return render(request, 'story3HOBBY.html')
def htmlcode(request):
    return render(request, 'story3HTMLcode.html')
def pendidikan(request):
    return render(request, 'story3PENDIDIKAN.html')
def MatKul(request):
    if request.method ==  "POST":
        form1 = MataKuliahForms(request.POST)
        if form1.is_valid():
            form1.save()
            return redirect('homepage:matakuliah')
    else :      
        form = MataKuliahForms()
        return render(request, 'story5MatKul.html',{"form_html" : form,"list_matkul" :  MataKuliah.objects.all()})
def kegiatan(request):
    if request.method == "POST":
        form1 = KegiatanForms(request.POST)
        if form1.is_valid():
            form1.save()
            return redirect('homepage:kegiatan')
    if len(Kegiatan.objects.all()) == 0 :
        forms1 = KegiatanForms()
        forms2 = ""
    else :
        forms1 = KegiatanForms()
        forms2 = PengikutForms()
    return render(request, 'Story6Kegiatan.html',{"form_html1" : forms1,"form_html2" : forms2,"list_kegiatan" :  Kegiatan.objects.all()})

def addpengikut(request,id_add):
    namaPengikutBaru = request.POST["namaPengikut"]
    kegiatanBaru = Kegiatan.objects.get(id = id_add)
    pengikut = Pengikut.objects.create(namaPengikut = namaPengikutBaru , kegiatan = kegiatanBaru)
    return redirect('homepage:kegiatan')

def books(request):
    return render(request,'SearchBarBooks.html')

def signup(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = User.objects.create_user(username = username , password = password)
        return redirect('homepage:HTMLcode')
    else :
        return render(request,'Signup.html')

def signin(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username = username , password = password)
        if user != None :
            login(request,user)
            
            return redirect('homepage:HTMLcode')
        else :
            return redirect('homepage:signin')
    else :
        return render(request,'Signin.html')
    
def logout1(request):
    logout(request)
    return redirect('homepage:HTMLcode')
 
def delete(request,id_delete):
    MataKuliah.objects.filter(id = id_delete).delete()
    return redirect('homepage:matakuliah')
           