from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    id = models.AutoField(primary_key=True,)
    namaMatkul = models.CharField(max_length = 50)
    namaDosen = models.CharField(max_length = 50)
    jumlahSks = models.IntegerField()
    descMatkul = models.CharField(max_length = 50)
    semester = models.IntegerField()
    ruangKelas = models.CharField(max_length = 50)

class Kegiatan(models.Model):
    id = models.AutoField(primary_key=True,)
    namaKegiatan = models.CharField(max_length = 50)

class Pengikut(models.Model):
    namaPengikut = models.CharField(max_length = 50)
    kegiatan = models.ForeignKey(Kegiatan,on_delete=models.CASCADE)
    
    
    
    