$( function() {
    $( "#accordion" ).accordion();
} );
 function move(element,direction ){
   
    let selected = element.parentNode,
    sibling = selected.nextElementSibling,
    parent = selected.parentNode;
    
 
    if (direction == -1 && selected.previousElementSibling) {
        
        parent.insertBefore(selected, selected.previousElementSibling.previousElementSibling);
        parent.insertBefore(sibling,sibling.previousElementSibling.previousElementSibling);
    } else if (direction == 1 && selected.nextElementSibling.nextElementSibling) {
        
        parent.insertBefore(selected, selected.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling);
        parent.insertBefore(sibling,sibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling);
    }
} 